package com.jellyfish.ar

import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.SurfaceView

import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.jellyfish.ar.arservice.picsinfo.PicsInfo
import com.yue.ar.coffee.camera.AndroidCameraController

class AndroidLauncher : AndroidApplication() {

    private var origWidth : Int = 0
    private var origHeight  : Int = 0
    private val TAG = "AndroidLauncher"
    lateinit var cameraControl : CameraControl
    val it2PicInfo : Intent = Intent()

    val APP_CAMERA: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        處理權限()
        初始化設定()
        it2PicInfo.setClass(this, PicsInfo::class.java)
        context.startService(it2PicInfo)

//        val config = AndroidApplicationConfiguration()
//        config.r = 8
//        config.g = 8
//        config.b = 8
//        config.a = 8
//        config.useImmersiveMode = false
//        config.hideStatusBar = false
//
//        cameraControl = AndroidCameraController(this)
//
//        initialize(App(cameraControl), config)
//
//        if (graphics.view is SurfaceView) {
//            val glView = graphics.view as SurfaceView
//
//            glView.holder.setFormat(PixelFormat.TRANSLUCENT)
//        }
//
//        // 設定 螢幕不會關閉
//        graphics.view.keepScreenOn = true
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()

        Log.d(TAG,"onResume")
        初始化設定()

//        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
//
//        val config = AndroidApplicationConfiguration()
//        config.r = 8
//        config.g = 8
//        config.b = 8
//        config.a = 8
//        config.useImmersiveMode = false
//        config.hideStatusBar = false
//
//        cameraControl = AndroidCameraController(this)
//        initialize(App(cameraControl), config)
//
//        if (graphics.view is SurfaceView) {
//            val glView = graphics.view as SurfaceView
//
//            glView.holder.setFormat(PixelFormat.TRANSLUCENT)
//        }
//
//        graphics.view.keepScreenOn = true
//
//        origHeight = graphics.height
//        origWidth = graphics.width

    }

    fun restoreFixedSize() {
        if (graphics.view is SurfaceView) {
            val glView = graphics.view as SurfaceView
            glView.holder.setFormat(PixelFormat.TRANSLUCENT)
            glView.holder.setFixedSize(origWidth, origHeight)
        }
    }

    fun post(r: Runnable) {
        handler.post(r)
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun 處理權限(){
        Log.d(TAG,"處理權限")
        if(checkSelfPermission(android.Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){

        requestPermissions(arrayOf(android.Manifest.permission.CAMERA),APP_CAMERA)
        }
    }

    fun 初始化設定(){
        Log.d(TAG,"初始化設定")
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        val config = AndroidApplicationConfiguration()
        config.r = 8
        config.g = 8
        config.b = 8
        config.a = 8
        config.useImmersiveMode = false
        config.hideStatusBar = false

        cameraControl = AndroidCameraController(this)
        initialize(App(cameraControl), config)

        if (graphics.view is SurfaceView) {
            val glView = graphics.view as SurfaceView

            glView.holder.setFormat(PixelFormat.TRANSLUCENT)
        }

        graphics.view.keepScreenOn = true

        origHeight = graphics.height
        origWidth = graphics.width
    }

}