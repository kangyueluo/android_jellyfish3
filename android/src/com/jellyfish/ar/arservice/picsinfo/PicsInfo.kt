package com.jellyfish.ar.arservice.picsinfo

import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.os.IBinder
import android.util.Log
import java.util.*

class PicsInfo : Service() {

    val TAG : String = "PicsInfo"
    var arImages : ArrayDeque<Bitmap> = ArrayDeque()
    var frameQueue : ArrayDeque<ByteArray> = ArrayDeque()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        載入arImage()
        picInfoAR()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    /**
     * 從 assets 載入 ar圖片
     */
    fun 載入arImage() {
        Log.d(TAG,"載入arImage")


    }

    /**
     * picInfoAR 算法
     */
    fun picInfoAR(){
        Log.d(TAG,"picInfoAR")
    }
}
